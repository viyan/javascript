/**
 * Tic Tac Toe game
 * @author: Amargi
 * @version: 1.0, 2024-12-01
 * @url: https://www.amargi.dev
 * @demo: https://www.amargi.dev/js/tic-tac-toe
 *
 */

const winnerRows = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
    [1, 4, 7],
    [2, 5, 8],
    [3, 6, 9],
    [1, 5, 9],
    [3, 5, 7],
  ],
  corners = [1, 3, 7, 9],
  sides = [2, 4, 6, 8],
  computerName = "Amargi",
  player1Name = "Player 1",
  player2Name = "Player 2";
var players = {};
var isOnePlayer = false;
var playerTurn, played, stage;

window.addEventListener("load", (event) => {
  startNewGame();
});

function getElementValue(elementId) {
  return document.getElementById(elementId).innerHTML;
}

function setElementValue(elementId, value) {
  document.getElementById(elementId).innerHTML = value;
}

function setElementClass(elementId, className) {
  document.getElementById(elementId).className = className;
}

function removeElementClass(elementId, className) {
  document.getElementById(elementId).removeAttribute("class", className);
}

function getInputValue(elementId) {
  return document.getElementById(elementId).value;
}

function setInputValue(elementId, value) {
  document.getElementById(elementId).value = value;
}

function setStep(step) {
  for (let i = 1; i <= 3; i++) {
    let visible = (i === step);
    let elementId = "step-" + i;
    document.getElementById(elementId).style.display = visible
      ? "revert"
      : "none";
  }
}

function setPlayers(player1, player2) {
  playerTurn = Math.floor(Math.random() * 2);
  let [value1, value2] = (playerTurn === 1) ? ["O", "X"] : ["X", "O"];
  players = {
    1: {
      name: player1,
      value: value1,
    },
    2: {
      name: player2,
      value: value2,
    },
  };
}

function startNewGame(reset=false) {
  setElementValue("game-info", "New Game!");
  removeElementClass("player-1", "current-player");
  removeElementClass("player-2", "current-player");
  setElementValue("player-1", "");
  setElementValue("player-2", "");
  stage = 1;
  played = 0;
  for (let i = 1; i <= 9; i++) {
    setElementValue(i, "");
    setElementClass(i, "");
  }
  let [player1, player2] = players[2]
    ? [players[1].name, players[2].name]
    : [player1Name, player2Name];
  setPlayers(player1, player2);
  if (reset) startGame();
  else setStep(1);
}

function selectPlayers(playersNum) {
  isOnePlayer = playersNum === 1;
  setInputValue("player-1-name", players[1].name);
  setInputValue("player-2-name", players[2].name);
  if (isOnePlayer) {
    setInputValue("player-2-name", computerName);
    setElementClass("player-2-name", "computer-player");
  } else {
    removeElementClass("player-2-name", "computer-player");
  }
  setStep(2);
}

function startGame() {
  for (let i = 1; i <= 2; i++) {
    players[i].name = getInputValue(`player-${i}-name`);
    setElementValue(`player-${i}`, `${players[i].name}: ${players[i].value}`);
  }
  switchPlayer();
  setStep(3);
}

function selectThis(element) {
  let elementId = element.id;
  selectById(elementId, false);
}

function selectById(elementId, computerPlays) {
  if (
    getElementValue(elementId) ||
    stage === 3 ||
    (isOnePlayer && !computerPlays && playerTurn === 2)
  )
    return;
  if (stage === 1) {
    setElementValue("game-info", "Game Started!");
    stage = 2;
  }
  changeValueTo(elementId, players[playerTurn].value);
  let winnerRow = checkWinner();
  if (winnerRow) {
    for (let row of winnerRow) {
      setElementClass(row, "winner");
    }
    setElementValue("game-info", `${players[playerTurn].name} won!`);
    stage = 3;
  }
}

function switchPlayer() {
  if (playerTurn === 1) {
    removeElementClass("player-1", "current-player");
    playerTurn = 2;
    if (isOnePlayer) {
      setTimeout(autoSelect, 1000);
    }
  } else {
    removeElementClass("player-2", "current-player");
    playerTurn = 1;
  }
  setElementClass(`player-${playerTurn}`, "current-player");
}

function changeValueTo(elementId, value) {
  setElementValue(elementId, value);
  setElementClass(elementId, "selected" + value);
  played += 1;
}

function checkWinner() {
  let winnerRow = 0;
  for (let row of winnerRows) {
    if (
      getElementValue(row[0]) &&
      getElementValue(row[0]) === getElementValue(row[1]) &&
      getElementValue(row[0]) === getElementValue(row[2])
    ) {
      winnerRow = row;
      break;
    }
  }
  if (played === 9) {
    noWinnerEndGame();
  }
  if (!winnerRow && played !== 9) switchPlayer();
  return winnerRow;
}

function noWinnerEndGame() {
  setElementValue("game-info", "No winner!");
  stage = 3;
}

// Computer play
function autoSelect() {
  let selectedByComputer;
  
  // First move strategy
  if (played === 0) {
    // Always take center if going first - strongest opening move
    selectedByComputer = 5;
  } 
  // Second move strategy
  else if (played === 1) {
    if (getElementValue(5) === "") {
      // Take center if available as second move
      selectedByComputer = 5;
    } else {
      // If center is taken, take a corner
      selectedByComputer = [1, 3, 7, 9][Math.floor(Math.random() * 4)];
    }
  }
  // Third move and beyond strategy
  else {
    // Priority 1: Win if possible
    selectedByComputer = getNextMove(2, 1);
    
    // Priority 2: Block opponent's winning move
    if (!selectedByComputer) {
      selectedByComputer = getNextMove(1, 1);
    }
    
    // Priority 3: Create a fork (two winning paths)
    if (!selectedByComputer) {
      selectedByComputer = createFork();
    }
    
    // Priority 4: Block opponent's fork
    if (!selectedByComputer) {
      selectedByComputer = blockFork();
    }
    
    // Priority 5: Take center, corner, or side
    if (!selectedByComputer) {
      selectedByComputer = getStrategicMove();
    }
  }
  
  if (selectedByComputer) selectById(selectedByComputer, true);
}

function getNextMove(player, step) {
  const value = (player === 2) ? players[1].value : players[2].value;
  
  for (let row of winnerRows) {
    const values = row.map(pos => getElementValue(pos));
    if (values.includes(value)) continue;
    
    if (step === 1) {
      const winCol = getWinningColumn(...values);
      if (winCol) return row[winCol - 1];
    } else {
      // Find first empty column
      const emptyIndex = values.findIndex(v => !v);
      if (emptyIndex !== -1) return row[emptyIndex];
    }
  }
  return 0;
}

function getWinningColumn(col1, col2, col3) {
  if (col1.length + col2.length + col3.length !== 2) return 0;
  
  if (!col3 && col1 === col2) return 3;
  if (!col2 && col1 === col3) return 2;
  if (!col1 && col2 === col3) return 1;
  return 0;
}

function createFork() {  
  // Check for potential fork opportunities
  for (let corner of corners) {
    if (getElementValue(corner) === players[2].value) {
      // Look for empty opposite corner
      let oppositeCorner = 10 - corner;
      if (getElementValue(oppositeCorner) === "") {
        return oppositeCorner;
      }
    }
  }
  return 0;
}

function blockFork() {
  let emptyCorners = corners.filter(corner => getElementValue(corner) === "");
  let emptySides = sides.filter(side => getElementValue(side) === "");
  
  // Block potential corner fork
  if (getElementValue(5) === players[2].value) {
    if (emptyCorners.length >= 2) {
      // Take a side to prevent corner fork
      return emptySides[0] || 0;
    }
  }
  
  return 0;
}

function getStrategicMove() {
  // Prefer center
  if (getElementValue(5) === "") return 5;
  
  // Then prefer corners
  for (let corner of corners) {
    if (getElementValue(corner) === "") return corner;
  }
  
  // Finally take any available side
  for (let side of sides) {
    if (getElementValue(side) === "") return side;
  }
  
  return 0;
}
