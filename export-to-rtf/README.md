# Export to RTF file
Use JavaScript to export data and items to RTF file using a template.

## Usage

``` javascript
<script type="text/javascript">
  exportDataToRTF(data, items); // to export both data and items
  exportDataToRTF(data, null);  // to export data without items
</script>
```

