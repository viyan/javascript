/**
 * Use JavaScript to export data and items to RTF file using a template.
 *
 * @author amargi.dev
 * @version 1.0, 2024-11-08
 *
 * Examples of use:
 *   exportDataToRTF(data, items);
 *   exportDataToRTF(data, null);
 */

var serverAddress = "localhost";
var templatePath = serverAddress + "/path";
var date = new Date();
var currentDate = date.toLocaleString("sv-SE")

var data = {
    "RtfTemplateFile": "template.rtf",
    "ExportFileName": "exported-file.rtf",
    "CONTACT_NAME": "Fristname Lastname",
    "STREET_ADDRESS": "Some Street 1",
    "ZIP_CODE": "00980",
    "COUNTRY": "Finland",
    "CONTACT_PHONE": "+358 10 1234 5678",
    "CONTACT_EMAIL": "usermail@nomail.no",
    "DATE": currentDate
};

var items = [
    {
        "ITEM_NAME": "Item no. #1",
        "_ITEM_DESCRIPTION": "Description with <u>underline</u> text.",
        "ITEM_PRICE": "90,00 €",
        "ITEM_QUANTITY": "3,00",
        "ITEM_TOTAL": "270,00 €"
    },
    {
        "ITEM_NAME": "Item no. #2",
        "_ITEM_DESCRIPTION": "Description with <b>bold</b> text.",
        "ITEM_PRICE": "70,00 €",
        "ITEM_QUANTITY": "2,00",
        "ITEM_TOTAL": "140,00 €"
    },
    {
        "ITEM_NAME": "Item no. #3",
        "_ITEM_DESCRIPTION": "Description with <i>italic</i> text.",
        "ITEM_PRICE": "30,00 €",
        "ITEM_QUANTITY": "4,00",
        "ITEM_TOTAL": "120,00 €"
    }
];

function exportDataToRTF(loadData, itemsData) {
    var rtfFileUrl = templatePath + '/' + loadData['RtfTemplateFile'],
        outputFileName = loadData['ExportFileName'], rtfContent;
    rtfFileUrl = rtfFileUrl.replaceAll('%3F', '?');
    fetch(rtfFileUrl)
        .then(function (response) {
            if (!response.ok) {
                throw new Error('Network response was not OK.');
            }
            return response.text();
        })
        .then(function (rtfTemplate) {
            rtfContent = replacePlaceHolders(rtfTemplate, loadData);
            if (itemsData) {
                rtfContent = replaceItemsPlaceHolders(rtfContent, itemsData);
            }
            var blob = new Blob([rtfContent], { type: 'application/rtf;charset=utf-8' });
            var link = document.createElement('a');
            link.href = URL.createObjectURL(blob);
            link.download = outputFileName;
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        })
        .catch(function (error) {
            console.error('There was a problem with the fetch operation:', error);
        });
}

function replacePlaceHolders(rtfContent, data) {
    var regex, keyName, value;
    Object.keys(data).forEach(function(key) {
        (key.toString().startsWith('_')) ?
            (keyName = key.toString().substring(1), value = convertHtmlToRtf(data[key])) :
            (keyName = key.toString(), value = data[key]);
        value = replaceSpecialCharactersWithUnicode(value);
        regex = new RegExp('\\[' + keyName + '\\]', 'g');
        rtfContent = rtfContent.replace(regex, value);
    });
    return rtfContent;
}

function replaceItemsPlaceHolders(rtfContent, data) {
    var regex, keyName, value, rowItems = '';
    var rowRegex = new RegExp('(\\\\trowd(?:(?!\\\\trowd)[\\s\\S])*?\\[' + Object.keys(data[0])[0] + '\\][\\s\\S]*?\\\\row)', 'g');
    var rowTemplate = rtfContent.match(rowRegex);
    if (!rowTemplate) return rtfContent;
    for (var i = 0; i < data.length; i++) {
        rowItems += rowTemplate[0];
        Object.keys(data[i]).forEach(function(key) {
            (key.toString().startsWith('_')) ?
                (keyName = key.toString().substring(1), value = convertHtmlToRtf(data[i][key])) :
                (keyName = key.toString(), value = data[i][key]);
            value = replaceSpecialCharactersWithUnicode(value);
            regex = new RegExp('\\[' + keyName + '\\]', 'g');
            rowItems = rowItems.replace(regex, value);
        });
    }
    return rtfContent.replace(rowRegex, rowItems);
}

function replaceSpecialCharactersWithUnicode(input) {
    if (!(typeof input === "string" && input)) {
        return '';
    }
    return input.toString().split('').map(function (value) {
        var code = value.charCodeAt(0);
        if (code === 160) {
            return " ";
        } else if (code > 127) {
            return '\\u' + code.toString().toUpperCase();
        }
        return value;
    }).join('');
}

/* 
 * Copied the code for converting HTML to RTF from stackoverflow
 * https://stackoverflow.com/questions/29299632/
 * Added support for headers (h1, h2, h3, h4, h5, h6), ordered lists and unordered lists.
 */
function convertHtmlToRtf(html) {
    if (!(typeof html === "string" && html)) {
        return '';
    }

    var tmpRichText, hasHyperlinks;
    var richText = html;

    // Delete HTML comments
    richText = richText.replace(/<!--[\s\S]*?-->/ig,"");

    // Singleton tags
    richText = richText.replace(/<(?:hr)(?:\s+[^>]*)?\s*[\/]?>/ig, "{\\pard \\brdrb \\brdrs \\brdrw10 \\brsp20 \\par}\n{\\pard\\par}\n");
    richText = richText.replace(/<(?:br)(?:\s+[^>]*)?\s*[\/]?>/ig, "{\\pard\\par}\n");

    // Empty tags
    richText = richText.replace(/<(?:p|div|section|article)(?:\s+[^>]*)?\s*[\/]>/ig, "{\\pard\\par}\n");
    richText = richText.replace(/<(?:[^>]+)\/>/g, "");

    // Hyperlinks
    richText = richText.replace(
        /<a(?:\s+[^>]*)?(?:\s+href=(["'])(?:javascript:void\(0?\);?|#|return false;?|void\(0?\);?|)\1)(?:\s+[^>]*)?>/ig,
        "{{{\n");
    tmpRichText = richText;
    richText = richText.replace(
        /<a(?:\s+[^>]*)?(?:\s+href=(["'])(.+)\1)(?:\s+[^>]*)?>/ig,
        "{\\field{\\*\\fldinst{HYPERLINK\n \"$2\"\n}}{\\fldrslt{\\ul\\cf1\n");
    hasHyperlinks = richText !== tmpRichText;
    richText = richText.replace(/<a(?:\s+[^>]*)?>/ig, "{{{\n");
    richText = richText.replace(/<\/a(?:\s+[^>]*)?>/ig, "\n}}}");

    // Start tags
    richText = richText.replace(/<(?:b|strong)(?:\s+[^>]*)?>/ig, "{\\b\n");
    richText = richText.replace(/<(?:i|em)(?:\s+[^>]*)?>/ig, "{\\i\n");
    richText = richText.replace(/<(?:u|ins)(?:\s+[^>]*)?>/ig, "{\\ul\n");
    richText = richText.replace(/<(?:strike|del)(?:\s+[^>]*)?>/ig, "{\\strike\n");
    richText = richText.replace(/<sup(?:\s+[^>]*)?>/ig, "{\\super\n");
    richText = richText.replace(/<sub(?:\s+[^>]*)?>/ig, "{\\sub\n");
    richText = richText.replace(/<(?:p|div|section|article)(?:\s+[^>]*)?>/ig, "{\\pard\n");
    richText = richText.replace(/<(?:h1|h2|h3|h4|h5|h6)(?:\s+[^>]*)?>/ig, "{\\pard\\par}{\\pard\\b\n");
    richText = richText.replace(/<(?:ol)(?:\s+[^>]*)?>/ig,
        "{\\pard\\par}{{\\*\\pn\\pnlvlbody\\pnindent0\\pnstart1\\pndec{\\pntxta.}}\\fi-240\\li720\\sa200\\sl180\\slmult1");
    richText = richText.replace(/<(?:ul)(?:\s+[^>]*)?>/ig,
        "{\\pard\\par}{{\\*\\pn\\pnlvlblt\\pnf1\\pnindent0{\\pntxtb\\\'B7}}\\fi-240\\li720\\sa200\\sl180\\slmult1");
    richText = richText.replace(/<(?:li)(?:\s+[^>]*)?>/ig, "{\\pntext\\tab}");

    // End tags
    richText = richText.replace(/<\/(?:p|div|section|article)(?:\s+[^>]*)?>/ig, "\n\\par}\n");
    richText = richText.replace(/<\/(?:h1|h2|h3|h4|h5|h6)(?:\s+[^>]*)?>/ig, "\n\\par}\n");
    richText = richText.replace(/<\/(?:b|strong|i|em|u|ins|strike|del|sup|sub|ol|ul)(?:\s+[^>]*)?>/ig, "\n}");
    richText = richText.replace(/<\/(?:li)(?:\s+[^>]*)?>/ig, "\\par");

    // Strip any other remaining HTML tags [but leave their contents]
    richText = richText.replace(/<(?:[^>]+)>/g, "");

    // Remove empty line at the beginning of the text
    richText = richText.startsWith("{\\pard\\par}")  ? richText.substring(11) : richText;

    // Prefix and suffix the rich text with the necessary syntax
    richText =
        "{\\rtf1\\ansi\n" + (hasHyperlinks ? "{\\colortbl\n;\n\\red0\\green0\\blue255;\n}\n" : "") + richText +
        "\n}";

    return richText;
}
