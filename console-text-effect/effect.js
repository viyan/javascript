/**
 * Console/terminal text effect
 *
 * Original code copied from:
 *    https://www.proglobalbusinesssolutions.com/css-hover-effects/
 *    https://codepen.io/Tbgse/pen/dYaJyJ
 *
 * @modified: amargi.dev
 * @version: 1.0, 2024-02-13
 * @demo: https://www.amargi.dev/term/
 * Examples of use:
 * function(lines['line1', 'line2'], 'targetId', speed, repeat);
 */

function consoleText(lines, id, speed, repeat) {
  var letterCount = 1;
  var x = 1;
  var waiting = false;
  var target = document.getElementById(id);

  var typingsetInterval = window.setInterval(function() {
    if (letterCount === 0 && waiting === false) {
      waiting = true;
      target.innerHTML = lines[0].substring(0, letterCount)
      window.setTimeout(function() {
        var line = lines.shift();
        if (repeat) {
          lines.push(line);
        }
        if (!lines.length) {
          clearInterval(typingsetInterval);
        }
        x = 1;
        letterCount += x;
        waiting = false;
      }, speed)
    } else if (letterCount === lines[0].length + 1 && waiting === false && (lines.length > 1)) {
      waiting = true;
      window.setTimeout(function() {
        x = -1;
        letterCount += x;
        waiting = false;
      }, 500)
    } else if (waiting === false) {
      target.innerHTML = lines[0].substring(0, letterCount)
      letterCount += x;
    }
  }, speed)
}
