# Console Text Effect
Original code copied from: https://www.proglobalbusinesssolutions.com/css-hover-effects/ \
And modified by Amargi (amargi.dev)

## Usage

``` javascript
<script type="text/javascript">
  function(lines['line1', 'line2'], 'targetId', speed, repeat);
</script>
```
Where\
**lines** is array of lines to be printed,\
**targetId** is the element id of span where lines will be printed,\
**speed** is the speed of typing in miliseconds,\
**repeat** repeat infinitely? (0 = false, 1 = true)

## Demo
https://amargi.dev/term
